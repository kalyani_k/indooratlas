function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection,md5) {
    router.get("/",function(req,res){
    	//var id = req.params.id;
    	//console.log(id);
    	connection.query('SELECT  * from geoPoints'
    		, function(err, rows, fields) {
			connection.end();
	  		if (!err){
	  	 		res.json(rows);
	    		console.log('The solution is: ', rows);
			}
	  		else
	    		console.log('Error while performing Query.');
	  	});
    });
    router.get("/:id",function(req,res){
    	var name = req.params.id;
    	console.log(name);
    	connection.query('SELECT  *from geoPoints where id = ?',[name]
    		, function(err, rows, fields) {
			connection.end();
	  		if (!err){
	  	 		res.json(rows);
	    		console.log('The solution is: ', rows);
			}
	  		else
	    		console.log('Error while performing Query.');
	  	});
    })
}

module.exports = REST_ROUTER;